package l.drizzd.googledrivedemo

import android.app.Activity
import android.content.Intent
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

abstract class BaseActivity: AppCompatActivity() {
    private val ongoingActivities = mutableMapOf<Int, Continuation<Intent?>>()

    protected suspend fun runActivity(intent: Intent, requestCode: Int): Intent? = suspendCoroutine { cont ->
        ongoingActivities[requestCode] = cont
        startActivityForResult(intent, requestCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        ongoingActivities[requestCode]?.let {
            if (resultCode == Activity.RESULT_OK) {
                it.resume(data)
            } else if (resultCode == Activity.RESULT_CANCELED) {
                it.resumeWithException(IllegalStateException("Activity cancelled"))
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}