package l.drizzd.googledrivedemo

import com.google.api.client.http.AbstractInputStreamContent
import com.google.api.services.drive.Drive
import com.google.api.services.drive.model.File
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class DriveServiceHelper(private val drive: Drive) {
    suspend fun create(
        metadata: File,
        content: AbstractInputStreamContent
    ): File = withContext(Dispatchers.IO) {
        drive.files().create(metadata, content)
            .setFields("id")
            .execute()
    }
}