package l.drizzd.googledrivedemo

import android.os.Bundle
import android.util.Log
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.Scope
import com.google.api.client.extensions.android.http.AndroidHttp
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.http.ByteArrayContent
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.gson.GsonFactory
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.drive.Drive
import com.google.api.services.drive.DriveScopes
import com.google.api.services.drive.model.File
import kotlinx.coroutines.*
import java.io.InputStreamReader
import java.util.*
import kotlin.coroutines.CoroutineContext
import kotlin.random.Random

class MainActivity : BaseActivity(), CoroutineScope {
    private val job: Job = SupervisorJob()
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    companion object {
        @JvmField val TAG: String = MainActivity::class.java.simpleName
        const val REQUEST_CODE_SIGN_IN = 1
    }

    private lateinit var driveService: DriveServiceHelper
    private lateinit var googleAccount: GoogleSignInAccount

    private val applicationName: String
        get() = getString(R.string.app_name)

    private val clientSecrets: GoogleClientSecrets by lazy {
        GoogleClientSecrets.load(
            JacksonFactory.getDefaultInstance(),
            InputStreamReader(resources.openRawResource(R.raw.client_secret))
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        launch {
            requestSignIn()
            initializeDriveService()
            createFile()
            initializeDriveServiceViaAuthCode()
            createFile()
        }
    }

    private suspend fun requestSignIn() {
        val signInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .requestScopes(Scope(DriveScopes.DRIVE_FILE))
            .requestServerAuthCode(clientSecrets.details.clientId)
            .build()
        val client = GoogleSignIn.getClient(this, signInOptions)
        val result = runActivity(client.signInIntent, REQUEST_CODE_SIGN_IN)
        googleAccount = GoogleSignIn.getSignedInAccountFromIntent(result).await()
        Log.d(TAG, "Signed in as " + googleAccount.email!!)
        Log.d(TAG, "Server auth code " + googleAccount.serverAuthCode)
    }

    private suspend fun initializeDriveServiceViaAuthCode() = withContext(Dispatchers.IO) {
        val tokenResponse = GoogleAuthorizationCodeTokenRequest(
            NetHttpTransport(),
            JacksonFactory.getDefaultInstance(),
            "https://oauth2.googleapis.com/token",
            clientSecrets.details.clientId,
            clientSecrets.details.clientSecret,
            googleAccount.serverAuthCode,
            ""
        )
            .execute()

        val credential = GoogleCredential().setAccessToken(tokenResponse.accessToken)
        val drive =
            Drive.Builder(NetHttpTransport(), JacksonFactory.getDefaultInstance(), credential)
                .setApplicationName(applicationName)
                .build()
        driveService = DriveServiceHelper(drive)
    }

    private fun initializeDriveService() {
        val credential = GoogleAccountCredential.usingOAuth2(
            this, Collections.singleton(DriveScopes.DRIVE_FILE)
        )
        credential.selectedAccount = googleAccount.account
        val drive = Drive.Builder(
            AndroidHttp.newCompatibleTransport(),
            GsonFactory(),
            credential
        )
            .setApplicationName(applicationName)
            .build()
        driveService = DriveServiceHelper(drive)
    }

    private suspend fun createFile() {
        val content = ByteArrayContent("text/plain", "hello".toByteArray())
        val randomString = Random.nextBytes(4).joinToString("") { "%02x".format(it) }
        val fileName = "${applicationName}WasHere$randomString.txt"
        val file = driveService.create(File().setName(fileName), content)
        Log.d(TAG, "File ID: ${file.id}")
    }
}
