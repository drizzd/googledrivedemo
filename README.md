# Google Drive Demo

## Set Up

1.  Install the [Android SDK](https://developer.android.com/sdk/index.html).
1.  Download and configure the
    [Google Play services SDK](https://developer.android.com/google/play-services/setup.html).
1.  Create a
    [Google API Console](https://console.developers.google.com/projectselector/apis/dashboard)
    project and enable the Drive API library.
1.  Register an OAuth 2.0 client for the package
    `l.drizzd.googledrivedemo` with your own
    [debug keys](https://developers.google.com/drive/android/auth).
1.  Add the `../auth/drive.file` scope to the OAuth consent screen in the API
    Console.
1.  Download the client secrets JSON from the [Credentials page](https://console.developers.google.com/apis/credentials)
    in the API Console and store it at `app/src/main/res/raw/client_secret.json`.

See full instructions in the
[Getting Started guide](https://developers.google.com/drive/android/get-started).
